;(function($, window, undefined) {
  'use strict';

  var pluginName = 'tabs';

  var myData = [];
      // myArr = [];

  var loadAjax = function(url) {
    var d = $.Deferred();
    $.ajax({
      url: url,
      type: 'GET',
    })
    .done(function(data) {
      console.log('success');
      d.resolve(data);
    })
    .fail(function() {
      console.log('error');
      d.reject('error');
    })
    .always(function() {
      console.log('complete');
    });
    return d.promise();
  };

  function containsKeyObject(key, list) {
    var i;
    for (i = 0; i < list.length; i++) {
      if (list[i].index === key) {
        return true;
      }
    }
    return false;
  }

  var handlerClickTab = function(self) {
    var siblings = self.siblings(),
    thisContent = self.data('item'),
    content = this.vars.content,
    index = self.index(),
    url = self.data('url');

    // setLocalstorage
    localStorage.active = index;

    content.each(function(){
      var dataContent = $(this).data('content');
      if(dataContent===thisContent){
        $(this).removeClass('hidden');
        $(this).addClass('in');

        var t = $(this);
        if(containsKeyObject(index, myData)) {
          myData.forEach(function(obj){
            if(obj.index === index) {
              t.text(obj.content);
            }
          });
        }
        else {
          loadAjax(url).then(function(data) {
            myData.push({index:index, content: data.content});
            t.text(data.content);
          });
        }

      }
      else {
        $(this).addClass('hidden in');
        $(this).removeClass('in');
      }
    });
    self.addClass('active');
    siblings.removeClass('active');
  };

  var loadTabStorage = function() {
    var headingItem = this.vars.headingItem,
        headingActiveItem = headingItem.eq(localStorage.active),
        content = this.vars.content,
        contentActive = content.eq(localStorage.active),
        url = headingActiveItem.data('url');

    if(localStorage.active !== undefined) {
      // load active heading
      headingItem.removeClass('active');
      headingActiveItem.addClass('active');
      // load active content
      content.addClass('hidden').removeClass('in');
      contentActive.removeClass('hidden').addClass('in');
      // load url data content
      loadAjax(url).then(function(data){
        myData.push({index:localStorage.active, content: data.content});
        contentActive.text(data.content);
      });
    }
    else {
      headingItem.eq(this.options.tabActive).addClass('active');
    }
  };

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this,
        el = that.element,
        // options = that.options,
        headingItem = el.find('[data-item]');

      this.vars = {
        headingItem: el.find('[data-item]'),
        content: el.find('[data-content]'),
        currentActive: el.find('.active')
      };

      loadTabStorage.call(this);

      headingItem.on('click', function() {
        var self = $(this);
        handlerClickTab.call(that, self);
      });
    },
    destroy: function() {
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      }
    });
  };

  $.fn[pluginName].defaults = {
    tabActive: 0
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]();
  });

}(jQuery, window));

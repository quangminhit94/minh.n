/**
 *  @name plugin
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
;(function($, window, undefined) {
  'use strict';

  var pluginName = 'select-box';

  var
  showLoading = function(self) {
    self.addClass('active').siblings('li').removeClass('active');
    $('[data-loading]')['loading']('showLoading');
    this.vars.selectedItem.text(self.text());
  },
  selectClickOut = function() {
    this.vars.items.slideUp(this.options.duration);
  },
  showHideSelect = function() {
    this.vars.items.slideToggle(this.options.duration);
  };

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      this.setupHtml();
      var that = this,
          el = that.element;
      var selectBox = el.next('[data-box]');
      this.vars = {
        selectedItem: selectBox.find('.selected-option'),
        items: selectBox.find('.option'),
        item: selectBox.find('.option').find('li')
      };

      el.on('click.' + pluginName, function() {
        showHideSelect.call(that, []);
      });

      el.on('focusout.' + pluginName, function() {
        selectClickOut.call(that, []);
      });

      that.vars.item.on('click.' + pluginName, function() {
        var self = $(this);
        showLoading.call(that, self);
      });

    },
    setupHtml: function() {
      var that = this,
          el = that.element,
          selectBox = el.find('select'),
          selectBoxItem = selectBox.find('option');

      var list = [],
          s = 1;
      // get all item push to a list
      selectBoxItem.each(function(){
        if( $(this).is(':selected') ){
          s = 1;
        }
        else{
          s = 0;
        }
        list.push({
          value: $(this).attr('value'),
          text: $(this).text(),
          selected: s,
        });
      });

      // render list to html
      var html = '';
      $.each(list, function(k, v){
        s = (v.selected === 1) ? 'active' : '';
        if(v.value === undefined) {
          v.value = 0;
        }
        if(v.value !== 0) {
          html += '<li value='+v.value+' class="'+s+'">'+v.text+'</li>';
        }
      });

      var selectHtml =
            '<div class="select-box" data-box style="margin-top:100px">' +
              '<span>Select Gender</span>' +
              '<div class="select" tabindex="1">' +
                '<div class="selected-option">' +
                  '<span>Nothing is selected</span>' +
                '</div>' +
                '<ul class="option">' + html +
                '</ul>' +
              '</div>' +
            '</div>';
      el.after(selectHtml);
    },
    destroy: function() {
      // remove events
      // deinitialize
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      }
    });
  };

  $.fn[pluginName].defaults = {
    duration: 100
  };

  $(function() {

    $('[data-' + pluginName + ']')[pluginName]();

  });

}(jQuery, window));
